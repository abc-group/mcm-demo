#!/bin/bash

MYPATH="$(dirname $(realpath $0))"
export PRMS="${MYPATH}/scripts/"

mkdir autodock_venv

if [ ! -f 'mgltools_x86_64Linux2_1.5.6.tar.gz' ]; then
    wget 'http://mgltools.scripps.edu/downloads/downloads/tars/releases/REL1.5.6/mgltools_x86_64Linux2_1.5.6.tar.gz'
fi
tar xzf mgltools_x86_64Linux2_1.5.6.tar.gz 
cd mgltools_x86_64Linux2_1.5.6
DISPLAY='' ./install.sh -d "$MYPATH/autodock_venv/opt"
rm -rf mgltools_x86_64Linux2_1.5.6
