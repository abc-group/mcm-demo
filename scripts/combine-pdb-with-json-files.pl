#!/usr/bin/perl -w

if(@ARGV!=3){
    die"usage: $0 rec-pdb-file lig-json-file complex-pdb-file\n";
}

$rec_pdb_file=shift(@ARGV);
$lig_json_file=shift(@ARGV);
$complex_pdb_file=shift(@ARGV);

open IN,"<$rec_pdb_file";
open OUT,">$complex_pdb_file";
$count=0;
while(<IN>){
    if(/ATOM/){
	printf OUT $_;
	$count++;
    }
}
close(IN);
$num_of_rec_atoms=$count;
printf"the number of receptor atoms: $num_of_rec_atoms\n";

$count=1;#start with one
open JIN, "<$lig_json_file";
while(<JIN>){
    $flag=0;

    if(/\"x\":\s(\-?\d+\.\d+)/){
	$x = $1;
    }elsif(/\"y\":\s(\-?\d+\.\d+)/){
	$y = $1;
    }elsif(/\"z\":\s(\-?\d+\.\d+)/){
	$z = $1;
	$flag++;
    }elsif(/\"name\"\:\s\"(.*?)\"/){
	#@input=split;
	#$atom_name=$input[1];
	#$atom_name =~ s/\"(.*?)\"/$1/;
	$atom_name=$1;
	#printf"atom name: $atom_name\n";
    }elsif(/\"residue\"\:\s\"(.*?)\"/){
	#@input=split;
	#$res_num=$input[1];
	#$res_num =~ s/\"(.*?)\"/$1/;
	$res_num=$1;
    }elsif(/\"segment\"\:\s\"(.*?)\"/){
	#@input=split;
	#$seg_name=$input[1];
	#$seg_name =~ s/\"(.*?)\"/$1/;
	$seg_name=$1;
    }

    if($flag==1){

	$num=length($atom_name);
	if($num<=3){
	    printf OUT "ATOM%7d  %-3s %s   %3d    %8.3f%8.3f%8.3f  1.00  0.00      %-s\n", $count, $atom_name, X00, $res_num, $x, $y, $z, $seg_name;   
	}elsif($num==4){
	    printf OUT "ATOM%7d %s %s   %3d    %8.3f%8.3f%8.3f  1.00  0.00      %-s\n", $count, $atom_name, X00, $res_num, $x, $y, $z, $seg_name;
	}else{
	    printf"FATAL ERROR: atom name $atom_name is too long $num\n";
	    die;
	}
	$count++;
    }
}
printf OUT "END\n";
close(JIN);
close(OUT);
printf"the output is saved to file $complex_pdb_file\n";


