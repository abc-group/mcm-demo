#!/bin/bash

usage()
{
cat << EOF
usage: $0 [options] rec-pdb-file lig-json-file

OPTIONS:
   -h      Show this message
   -b      ligand bound structure
   -d      don't clean
   -a      patch termini (peptide option)
   -m      transformational matrix file
   -n      use pdbnmd.pl instead of pdbchm.pl
   -r      rotation file
   -p      forcefield parameter file
   -t      rtf parameter file
   -f      ft file
   -e      any pdb structure located near the place, where we want root to be

EOF
}

# setup paths to location of scripts and parameter files
# for mcm-package make relative to the examples directory\
MYPATH=$(dirname $(realpath $0))
BINPATH=$MYPATH
echo "scripts path: $BINPATH"

ROTFILE=${PRMS}/rotsets/rot70k.0.0.6.jm.prm
PRMFILE=${PRMS}/parm_new.prm
RTFFILE=${PRMS}/pdbamino_new.rtf

CLEAN=0
PATCH_TERM=0
PDBCHM=1
BOUND_FLAG=0
LIG_BOUND=
TRFMTRX=
FTFILE=
ROOTPDB=
SKIPREC=

while getopts hf:b:r:p:t:e:m:dans option
do
        case "${option}"
        in
            h) usage 
               exit 1;;
	        b) LIG_BOUND=${OPTARG};;
	        d) CLEAN=0;;
            a) PATCH_TERM=1;;
            m) TRFMTRX=${OPTARG};;
	        n) PDBCHM=0;;
            r) ROTFILE=${OPTARG};;
	        p) PRMFILE=${OPTARG};;
            t) RTFFILE=${OPTARG};;
            f) FTFILE=${OPTARG};;
            e) ROOTPDB=${OPTARG};;
            s) SKIPREC=1;;
            ?) usage
	        exit 1;;
        esac
done

if [ $(( $# - $OPTIND )) -lt 1 ]; then
    usage
    exit 1
fi

REC=${@:$OPTIND:1}
LIG=${@:$OPTIND+1:1}

if [[ $LIG_BOUND ]]; then
    BOUND_FLAG=1;
fi

echo "receptor pdb file: $REC"
echo "ligand json file: $LIG"
echo "bound flag: $BOUND_FLAG"
echo "clean flag: $CLEAN"
echo "patch termini (peptide option): $PATCH_TERM"
echo "use pdbchm.pl: $PDBCHM"
echo "forcefield parameter file: $PRMFILE"
echo "forcefield rtf file: $RTFFILE"
echo "root pdb: $ROOTPDB"

if [[ ! -s $REC ]]; then
    echo FATAL ERROR: pdb file $REC is not found
    exit 1;
elif [[ ! -s $LIG ]]; then
    echo FATAL ERROR: json file $LIG is not found
    exit 1;
fi

RU=rec.pdb
LU=lig.pdb

LUJ=${LU%.pdb}.json
if [[ $LUJ != $LIG ]]; then
    cp $LIG $LUJ
fi


if [[ ! $SKIPREC == 1 ]]; then

if [[ $RU != $REC ]]; then # don't copy if pdb names are rec.pdb and lig.pdb
    cp $REC $RU
fi

#if [[ $LU != $LIG ]]; then
#    cp $LIG $LU
#fi

#add chain names if missing
${BINPATH}/add-chain-name-to-pdb3.pl $RU ${RU}.new segid
mv $RU ${RU}.back1
mv ${RU}.new $RU

#replace non-standard residues
${BINPATH}/replace-non-standard-residues-in-pdb.pl $RU ${RU}.new
mv $RU ${RU}.back2
mv ${RU}.new $RU

${BINPATH}/renumber-insertion-residues-in-pdb.py $RU > ${RU}.new
mv $RU ${RU}.back3
mv ${RU}.new $RU

########################### take care of receptor ###################
echo "running pdbprep.pl on receptor: $RU ..."
${BINPATH}/pdbprep.pl $RU

if [[ $PDBCHM == 1 ]]; then
    echo "running pdbchm.pl on receptor: $RU ..."
    if [[ $PATCH_TERM == 0 ]]; then
		${BINPATH}/pdbchm.pl -osuffix=_min --prm $PRMFILE --rtf $RTFFILE  $RU ?
    else
		${BINPATH}/pdbchm.pl -osuffix=_min --patch-term --prm $PRMFILE --rtf $RTFFILE $RU ?
    fi
else
    echo "running pdbnmd.pl on receptor: $RU ..."
    if [[ $PATCH_TERM == 0 ]]; then
		${BINPATH}/pdbnmd.pl -osuffix=_min --prm $PRMFILE --rtf $RTFFILE  $RU ?
    else
		${BINPATH}/pdbnmd.pl -osuffix=_min --patch-term --prm $PRMFILE --rtf $RTFFILE $RU ?
    fi
fi

fi

PSFRU=${RU%.pdb}_min.psf
PDBRU=${RU%.pdb}_min.pdb
MOL2RU=${RU%.pdb}_min.mol2

if [[ -s $PDBRU ]]; then
    echo successfully prepared receptor pdb file: $PDBRU
else
    echo FATAL ERROR: receptor pdb file $PDBRU was not built
    exit 1;
fi

if [[ -s $PSFRU ]]; then
    echo successfully prepared receptor psf file: $PSFRU
else
    echo FATAL ERROR: receptor psf file $PSFRU was not built
    exit 1;
fi

babel $PDBRU $MOL2RU &> babel_r.out

if [[ -s $MOL2RU ]]; then
    echo successfully prepared receptor mol2 file: $MOL2RU
else
    echo FATAL ERROR: receptor pdb file $MOL2RU was not built
    exit 1;
fi

########################### take care of ligand ###################

#add element names to pdb to fix json parametrization crashing for hydrogens with long names 
#${BINPATH}/add-element-name.pl $LU ${LU}.new
#if [[ -s ${LU}.new ]]; then
#    echo "successfully added element name to ligand"
#else
#    echo "FATAL ERROR: element name was not added"
#    exit 1
#fi
#mv $LU ${LU}.back
#mv ${LU}.new $LU

#${ATLASPATH}/bin/atlas_parameterize $LU $LUJ --dont-minimize

#if [[ -s $LUJ ]]; then
#    echo "successfully prepared json file: $LUJ"
#else
#    echo "FATAL ERROR: ligand json file $LUJ was not generated"
#    exit 1
#fi

PDBLU=${LU%.pdb}_min.pdb
${BINPATH}/json2pdb.pl $LUJ $PDBLU

#add element names to pdb to fix prepare_ligand4.py crashing for hydrogens with long names
${BINPATH}/add-element-name.pl $PDBLU ${PDBLU}.new
mv $PDBLU ${PDBLU}.back
mv ${PDBLU}.new $PDBLU

PDBLU_PDBQT=${PDBLU%.pdb}.pdbqt

if [[ $ROOTPDB ]]; then
	ROOTNAME=$(basename $ROOTPDB)
    pymol -Qc $ROOTPDB $PDBLU -d "set pdb_retain_ids, 1; select root, ${PDBLU%.pdb} near_to 1.0 of ${ROOTNAME%.pdb}; save root.pdb, root"
    ROOT_INDEX=$(sed -n 's/^ATOM[^0-9]*\([0-9]\+\).*/\1/p' root.pdb | head -n1)
    
    if [[ $ROOT_INDEX ]]; then
		echo "Index of the root: $ROOT_INDEX"
        (
            source ../autodock_venv/opt/bin/mglenv.sh &&
    		python ../autodock_venv/opt/MGLToolsPckgs/AutoDockTools/Utilities24/prepare_ligand4.py -l $PDBLU -R $ROOT_INDEX
        )
	else
		echo "FATAL ERROR: ROOT_INDEX is empty. Exiting ..."
		exit 1
	fi
else
    (
        source ../autodock_venv/opt/bin/mglenv.sh &&
    	python ../autodock_venv/opt/MGLToolsPckgs/AutoDockTools/Utilities24/prepare_ligand4.py -l $PDBLU
    )
fi


if [[ -s $PDBLU_PDBQT ]]; then
    echo successfully prepared ligand pdbqt file $PDBLU_PDBQT
else
    echo FATAL ERROR: file $PDBLU_PDBQT was not built
    exit 1
fi

NUM=$(tail -3 $PDBRU | head -1 | awk {'print $2'})
echo "the number of receptor atoms: $NUM"

${BINPATH}/cnvrt_tree.sh $PDBLU_PDBQT $PDBLU rigid_cluster.prm $(($NUM))

if [[ -s rigid_cluster.prm ]]; then
    echo "successfully prepared file rigid_cluster.prm"
else
    echo "FATAL ERROR: file rigid_cluster.prm.back was not prepared"
fi

mv rigid_cluster.prm rigid_cluster.prm.back

echo "0" > rigid_cluster.prm
echo "0" >> rigid_cluster.prm
echo "1" >> rigid_cluster.prm
cat rigid_cluster.prm.back >> rigid_cluster.prm
echo "0" >> rigid_cluster.prm

########################## take care of complex pdb file #################
PDB_COMPLEX=complex.pdb
${BINPATH}/combine-pdb-with-json-files.pl $PDBRU $LUJ $PDB_COMPLEX

############################ take care of ligand bound #################
if [[ $BOUND_FLAG == 1 ]]; then
    cp $LIG_BOUND lig_bound.pdb
    echo "copied ligand bound file to lig_bound.pdb"
fi

if [[ $TRFMTRX ]]; then
    cp $TRFMTRX transform_matrix.prm
    echo "copied transformation matrix file to  transform_matrix.prm"
fi

if [[ $FTFILE ]]; then # convert ft file to transformation file

    TM="transform_matrix_from_ft.prm"
    ${BINPATH}/convert-ft-to-mcm-transform-matrix.py $FTFILE --rotfile $ROTFILE > $TM

    if [[ -s $TM ]]; then
	echo "successfully prepared transformation matrix file $TM"
    else
	echo "FATAL ERROR: transformation matrix file $TM was not prepared"
    fi

fi

if [[ $CLEAN == 1 ]]; then
    echo cleaning up the directory...
    rm charmm.out
    rm babel_l.out
    rm babel_r.out
    rm babel_r_l.out
    rm ${RU}.back*
    rm ${LU}.back*
    rm ${PSFRU}.back
    rm ${PSFLU}.back
    rm ${PSF}.back
fi

echo all done!
