#!/usr/bin/perl -w

if(@ARGV!=2){
    die"usage: $0 json-file pdb-file\n";
}

$json_file=shift(@ARGV);
$pdb_file=shift(@ARGV);

$count=1;#pdb atoms start with one
open JIN, "<$json_file";
open OUT,">$pdb_file";
while(<JIN>){
    $flag=0;
    if(/\"x\":\s(\-?\d+\.\d+)/){
	$x = $1;
    }elsif(/\"y\":\s(\-?\d+\.\d+)/){
	$y = $1;
    }elsif(/\"z\":\s(\-?\d+\.\d+)/){
	$z = $1;
	$flag++;
    }elsif(/\"name\"\:\s\"(.*?)\"/){
	$atom_name=$1;
	#printf"atom name: $atom_name\n";
    }elsif(/\"residue\"\:\s\"(.*?)\"/){
	$res_num=$1;
    }elsif(/\"segment\"\:\s\"(.*?)\"/){
	$seg_name=$1;
    }elsif(/\"element\"\:\s\"(.*?)\"/){
	$element=$1;
	#printf"element: $element\n";
    }

    if($flag==1){
	$atom_name_length=length($atom_name);
	$element_length=length($element);
	if($atom_name_length<=3 && $element_length==1){
	    printf OUT "ATOM%7d  %-3s %s   %3d    %8.3f%8.3f%8.3f  1.00  0.00      %-4s%2s\n", $count, $atom_name, X00, $res_num, $x, $y, $z, $seg_name, $element;   
	}elsif($atom_name_length==4 || $element_length==2){
	    printf OUT "ATOM%7d %-4s %s   %3d    %8.3f%8.3f%8.3f  1.00  0.00      %-4s%2s\n", $count, $atom_name, X00, $res_num, $x, $y, $z, $seg_name, $element;
	}else{
	    printf"FATAL ERROR: atom name $atom_name is too long $atom_name_length\n";
	    die;
	}
	$count++;
    }
}
printf OUT "END\n";
close(JIN);
close(OUT);
printf"the output is saved to file $pdb_file\n";


