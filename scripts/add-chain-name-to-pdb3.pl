#!/usr/bin/perl -w

# "ter" flag can be used when multiple chains are separated by TER keyword. 
# at the same time chain names must be passed as the last argument to the script
# when "ter" keyword is encountered, the next chain name is obtained from the list

if(@ARGV<3){
    die"usage: $0 pdb-file-in pdb-file-out normal|force|segid|segid-force|ter chain-name(s)\n";
}

$pdbfile=shift(@ARGV);
$outfile=shift(@ARGV);
$flag=shift(@ARGV);
@chain_names = @ARGV;

printf"flag: $flag\n";
$num_of_chains = @chain_names;
printf"the number of chain names passed: $num_of_chains\n";

#if($flag eq "force"){
#    printf"I will replace if necessary the old chain name\n";
#}elsif($flag eq "segid"){
#    printf"I will use segid for chain name\n";
#}

#$name = substr($pdbfile,0,-4);
#$pdbfile_out = $pdbfile . ".new";

if($flag eq "ter"){
    $chain_new=shift(@chain_names);
}

open IN,"<$pdbfile";
open OUT,">$outfile";
$count=0;
$count_atom=0;
$chain_old=" ";
while(<IN>){
    if(m/ATOM|HETATM/){
	$chain=substr($_,21,1);
	if($chain eq " "){
	    $chain=substr($_,72,2);#in piper pdb columb 72 is for L or R and 73 is for chain id, therefore check both
	}
	if($flag eq "segid"){
	    $chain=substr($_, 21, 1);
	    if($chain eq " "){
		$beg=substr($_,0,21);
		$end=substr($_,22);
		$chain_new=substr($_,72,1);
		printf OUT "$beg$chain_new$end";
	    }else{
		printf OUT $_;
	    }
	}elsif($flag eq "segid-force"){
	    $beg=substr($_,0,21);
	    $end=substr($_,22);
	    $chain_new=substr($_,72,1);
	    printf OUT "$beg$chain_new$end";	    
	}elsif($flag eq "force"){
	    #printf"chain: $chain\n";
	    if($chain ne $chain_old && $count_atom>0){
		$count++;
	    }
	    $beg=substr($_,0,21);
	    $end=substr($_,22);
	    $chain_new = $chain_names[$count];
	    printf OUT "$beg$chain_new$end";
	    $chain_old=$chain;
	}elsif($flag eq "normal" && $chain eq " " ){
	    $beg=substr($_,0,21);
	    $end=substr($_,22);
	    $chain_new = $chain_names[$count];
	    printf OUT "$beg$chain_new$end";
	}elsif($flag eq "ter"){
	    $beg=substr($_,0,21);
	    $end=substr($_,22);
	    printf OUT "$beg$chain_new$end";
	}else{
	    printf OUT $_;
	}
	$count_atom++;
    }elsif(m/TER/){
	if($flag eq "ter"){
	    $chain_new=shift(@chain_names);
	    if($chain_new eq ""){
		die"ran out of chain names, add more\n";
	    }
	}
	printf OUT "TER\n";
    }else{
	printf OUT $_;
    }
}
close IN;
close OUT;
printf"output is saved to file: $outfile\n";
