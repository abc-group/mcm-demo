#!/usr/bin/perl -w

if(@ARGV != 2){
    die"usage: $0 pdb-file new-pdb-file\n";
}

$pdbfile = shift(@ARGV);
$outfile = shift(@ARGV);

$count=0;
open IN, "<$pdbfile";
open OUT, ">$outfile";
while(<IN>){
    if(/ATOM/){
	$tmp=substr($_,17, 3);
	if($tmp eq "MSE"){
	    $tmp2=substr($_,13, 2);
	    if($tmp2 eq "SE"){
		$begin=substr($_, 0, 13);
		$end=substr($_,20);
		$str=$begin . "SD  MET" . $end;
		$count++;
	    }else{
		$begin=substr($_, 0, 17);
		$end=substr($_,20);
		$str=$begin . "MET" . $end;
	    }	

	    printf OUT "$str";
	}else{
	    printf OUT $_;
	}
    }else{
	printf OUT $_;
    }
}
close(IN);
close(OUT);
printf"replaced $count MSE residues\n";
printf"output is saved to file $outfile\n";
