#!/bin/bash

MYPATH="$(dirname $(realpath $0))"

NAME=CatS_1
RECNAME=3kwn

DIR="${MYPATH}/${NAME}"
DEST=output
mkdir -p $DEST
cp "$DIR/00001.pdb" "$DEST/lig.pdb"
cp "$DIR/00001.json" "$DEST/lig.json"
cp "$DIR/lig_rmsd.pdb" "$DEST/lig_rmsd.pdb"
cp "$NAME/rec_${RECNAME}_water.pdb" "$DEST/rec.pdb"

cd $DEST

export PRMS="${MYPATH}/scripts/"
../scripts/prep.sh

# Increment all atom numbers in rigid_cluster
awk 'NR<4 {print $0; next} NR==4 {nclus = $1; print $0; next} NR<(4+nclus+1) {printf "%i ", $1; for (i = 2; i < 2+$1; i++) {printf "%i ", $i+1} ; print ""; next} NR<(4+2*nclus) {printf "%i  %i\n", $1+1, $2+1; next} {print }' rigid_cluster.prm > rigid_cluster1.prm

pymol -qc "$DIR/lig_rmsd.pdb" lig_min.pdb -d "set pdb_retain_ids, 1; select ref, lig_min near_to 1.0 of lig_rmsd; save lig_rmsd.pdb, ref"

cd -

